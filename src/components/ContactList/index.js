import React, {Component} from 'react'
import Contact from '../Contact/index'
import './style.css'

export default class ContactList extends Component {

	state = {
		activeContactId: null
	}

	render() {
		const contactElements = this.props.users.map((user, index) =>
			<div key = {user.id} className="contactItem" onClick = {this.handleActive.bind(this, user.id)}>
				<Contact user = {user} isActive = {this.state.activeCintactId === user.id} />
			</div>
		);

		return (
			<div className="contactList">
				{contactElements}
			</div>
		)
	}

	handleActive = activeCintactId => this.setState({
		activeCintactId
	})
}