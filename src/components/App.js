import React from 'react'
import ContactList from './ContactList/index'
import users from '../usersDemo'
import 'reset-css'

class App extends React.Component{

	render() {
		return (
			<div style={
				{
					display: 'flex',
					width: '100%',
					height: '100%',
					overflow: 'hidden'

				}
			}>
				<ContactList users = {users} />
			</div>
		)
	}
}

export default App